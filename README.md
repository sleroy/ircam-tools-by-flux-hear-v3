> - Retail Price: 99 $ / 85 €
> - [Premium member](https://www.ircam.fr/innovations/abonnements-du-forum/) Price: 49,50 $ / 42,50 € - [Ask for the voucher](https://shop.ircam.fr/index.php?controller=contact)
> - Distributed by [Flux::](https://shop.flux.audio/en_US/products/ircam-hear)
> - [Technical Support](https://support.flux.audio/portal/sign_in)
> - Get you trial [Try](https://shop.flux.audio/en_US/page/trial-request-information)
> - Software available by downloading the [Flux:: Center ](https://www.flux.audio/download/)
> - iLok.com user account is required (USB Smart Key is not required)


![Flux::](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/logos-flux%2Bircam-noir.png)

## Ircam HEar v3 – Binaural Encoding Tool ##
**AU 32 & 64 bit | VST 32 & 64 bit | AAX Native 32 & 64 bit* **

It relies on proven technology to model the various phenomena that occur when playing back audio material through a loudspeaker system.

This allows monitoring a full surround mix in situations when a surround-capable environment is not available or practical. Another typical use of HEar is doing precise checking of a mix, which is convenient with headphones as these provide a ‘surgical’ and very detailed, microscope-like rendering of the audio. HEar can also prove very useful in a project studio context, and whenever noise isolation is a concern, as it helps achieving a more realistic sound environment.

## Binaural technology ##

![Binaural](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/binaural.jpg)

Binaural technology encompasses methods for recording, processing, synthesizing and reproducing sound that are specifically designed to preserve tridimensional localisation properties.

In order to mimic the impression of a sound originating from a given incidence, it is sufficient to filter a mono signal, which on its own lacks any kind of spatial information, with both left and right HRTF filters. This constitutes the foundation of binaural synthesis.

Please note that the resulting signal is only meant to be listened to with headphones, and isn’t designed for a conventional stereo loudspeaker setup.

## Signal Routing and Speaker setup ##

A routing Matrix provides an overview of the mapping between the plugin’s inputs from the DAW track to the virtual speaker internal outputs, and a Speaker Mode specifying which virtual speaker configuration that should be emulated.

## Features ##

- Up to ten channels Input/Output with support for Dolby Atmos
- Pro Tools HD 7.1.2 & 7.0.2 Dolby Atmos Bus Support
- Space Presets, presenting a range of spaces with subtly different colorations
- Routing Matrix for mapping between the plugins inputs from the DAW track to the virtual speaker internal outputs
- Speaker Mode specifies the emulated virtual speaker configuration
- Speaker Width to control the width between virtual speakers, expressed in degrees, which allows for  narrowing or broadening the stereo image.
- Angle Shift to control the angle between the listener and the center of the virtual speakers
- HEar relies on HRTF filter measurements made using a KEMAR (Knowles Electronics Manikin For Acoustic Research) dummy head and torso simulation.

![IRCAM HEar](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/ircam_hear-v3-full.jpg)

## Compatibility & plugin formats ##

- **Windows** – 7 SP1, 8.1 and 10, all in 64 bits only
- **Mac** OS X (Intel) – All versions from 10.7.5 (64bit only)
- **Ircam HEar v3 is available in the following configurations:** AU Audio Units (64bit) / VST (64 bit) / AAX Native (64bit)
- **Max sample rate (depending on HOST):** 384 KHz
- **Maximum number of channels:** 10

## Hardware Specification ##

A graphic card fully supporting OpenGL 2.0 is required.

- **Windows:** If your computer has an ATi or NVidia graphics card, please assure the latest graphic drivers from the ATi or NVidia website are installed.
- **Mac OS X:** OpenGL 2.0 required – Mac Pro 1.1 & Mac Pro 2.1 are not supported.

## Software license requirements ##

In order to use the software, an iLok.com user account is required (the iLok USB Smart Key is not required).

VS3 license not included in Flux:: standard license. Additional VS3 license for Pyramix & Ovation Native/MassCore 32/64 bit versions available from [Merging Technologies](http://www.merging.com/sales).

> - [TUTO IRCAM HEAR: Multichannel to Binaural By Jean-Loup Pecquais](https://youtu.be/i7bHNvEpZi0) [(version française)](https://youtu.be/1GAVHgfjxgY)
